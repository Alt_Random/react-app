import React from 'react';
import s from './Sidebar.module.css';
import SidebarItem from './SidebarItem/SidebarItem';

const Sidebar = (props) => {
    debugger;
    let sb = props.getSidebars();
   let sidebarElement = sb.map((s) => {return <SidebarItem id={s.id} name={s.name} surname={s.surname} thumbnail={s.thumbnail} />}); 
    return (
        <div className={s.friendsBlock}>
            {sidebarElement}
        </div>);
}

export default Sidebar;