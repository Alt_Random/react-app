import React from "react";
import s from './ProfileInfo.module.css';
const ProfileInfo = () => {
    return (
        <div className={s.content}>
            <img className={s.avatar} src="https://www.zastavki.com/pictures/originals/2014/Auto___Lamborghini_Reliable_car_Lamborghini_Murcielago__063514_.jpg" alt="avatar" />
            <div className={s.description}>Profile description</div>

        </div>

    );
}
export default ProfileInfo;