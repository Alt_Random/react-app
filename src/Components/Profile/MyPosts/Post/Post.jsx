import React from "react";
import s from './Post.module.css';

const Post = (props) => {

    return (
        <div>

            <div className={s.posts}>
                <div> <img className={s.avatar} src="https://www.zastavki.com/pictures/originals/2014/Auto___Lamborghini_Reliable_car_Lamborghini_Murcielago__063514_.jpg" alt="avatar" />
                   {props.message}</div>
            </div>

            <div className={s.item}><a>-------------------------</a></div>
            <div>
                <span>like: {props.like}</span>
            </div>

        </div>

    );
}
export default Post;