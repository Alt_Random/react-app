let rerenderEntireTree = (state) => {
  console.log('state changed');
}

let state = {
  profilePage: {
    posts: [
      { id: 0, message: 'Hello World!', likesCount: 11 },
      { id: 1, message: 'Hi, how are you?', likesCount: 22 }
    ],
    newPostText: 'texttext'
  },
  dialogsPage: {
    messages: [
      { id: 0, sentId: 'you', message: 'Hello World!' },
      { id: 1, sentId: 'you', message: 'Hi, how are you?' },
      { id: 2, sentId: 'person', message: 'Oh my gosh' },
      { id: 3, sentId: 'person', message: 'Lol' }
    ],
    dialogs: [
      { id: 0, name: 'First' },
      { id: 1, name: 'Second' },
      { id: 2, name: 'Third' }
    ],
    newMessageText: 'textmessage',
    value:'props value'
  },
  sidebar: [
    { id: 0, name: 'Sara', surname: 'MacKey', thumbnail: "../../img/0.png" },
    { id: 1, name: 'John', surname: 'Yolo', thumbnail: './1.png' },
    { id: 2, name: 'Bill', surname: 'Brekham', thumbnail: '2.png' }
  ]

}

export const addPost = () => {
  debugger;
  let newPost = {
    id: 2,
    message: state.profilePage.newPostText,
    likesCount: 0
  };
  state.profilePage.posts.push(newPost);
  state.profilePage.newPostText = '';
  rerenderEntireTree(state);
}

export const addMessage = () => {
  debugger;
  let newMessage = {
    id: 4,
    sentId: 'you',
    message: state.dialogsPage.newMessageText
  };
  state.dialogsPage.messages.push(newMessage);
  state.dialogsPage.newMessageText = '';
  rerenderEntireTree(state);
}

export const updateNewMessageText = (newMessageText) => {
  state.dialogsPage.newMessageText = newMessageText;
  rerenderEntireTree(state);
}

export const updateNewPostText = (newPostText) => {
  state.profilePage.newPostText = newPostText;
  rerenderEntireTree(state);
}

export let subscribe = (observer) => {
rerenderEntireTree = observer;
}

export default state;