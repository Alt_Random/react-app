import React from 'react';
import s from './DialogItem.module.css';
import { NavLink } from 'react-router-dom';
// const DialogIten = () => {
//     let path = "dialogs/" + props.id; //props.state.dialogs.id;

//     return <div className={s.dialog + ' ' + s.active}>
//         <NavLink to={path}>{props.name}</NavLink>
//     </div>

const DialogItem = (props) => {
    let path = "dialogs/" + props.id;
    return (
        <div className={s.dialog + ' ' + s.active}> 
        <NavLink to={path}>{props.name}</NavLink>
        </div> //props.state.dialogs.id;

    );
}

export default DialogItem;