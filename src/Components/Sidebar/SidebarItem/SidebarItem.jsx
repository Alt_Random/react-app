import React from 'react';
import Sidebar from '../Sidebar';
import s from './SidebarItem.module.css'

const SidebarItem = (props) => {
   
    return (
        <div className={s.friendsBlock}>
<img className='avatar' alt='avatar' src={props.thumbnail} />
           {props.name}
        </div>);
}

export default SidebarItem;