export let store = {
    _subscriber() {
      console.log('none');
    },
    subscribe(observer) {
      this._subscriber = observer;
    },
    _state: {
      profilePage: {
        posts: [
          { id: 0, message: 'Hello World!', likesCount: 11 },
          { id: 1, message: 'Hi, how are you?', likesCount: 22 }
        ],
        newPostText: 'texttext'
      },
      dialogsPage: {
        messages: [
          { id: 0, sentId: 'you', message: 'Hello World!' },
          { id: 1, sentId: 'you', message: 'Hi, how are you?' },
          { id: 2, sentId: 'person', message: 'Oh my gosh' },
          { id: 3, sentId: 'person', message: 'Lol' }
        ],
        dialogs: [
          { id: 0, name: 'First' },
          { id: 1, name: 'Second' },
          { id: 2, name: 'Third' }
        ],
        newMessageText: 'textmessage'
      },
      sidebar: [
        { id: 0, name: 'Sara', surname: 'MacKey', thumbnail: "../../img/0.png" },
        { id: 1, name: 'John', surname: 'Yolo', thumbnail: './1.png' },
        { id: 2, name: 'Bill', surname: 'Brekham', thumbnail: '2.png' }
      ]
    },
    getState() {
      return this._state;
    },
    getPosts() {
      return this._state.profilePage.posts;
    },
    getDialogs() {
      return this._state.dialogsPage.dialogs;
    },
    getMessages() {
      debugger;
      return this._state.dialogsPage.messages;
    },
    getSidebars() {
      debugger;
      return this._state.sidebar;
    },
    setPost() { //addPost
      debugger;
      let newPost = {
        id: 2,
        message: this._state.profilePage.newPostText,
        likesCount: 0
      };
      this._state.profilePage.posts.push(newPost);
      this._state.profilePage.newPostText = '';
      this.subscribe(this._state);
    },
    setMesssage() { //addMessage
      let newMessage = {
        id: 4,
        sentId: 'you',
        message: this._state.dialogsPage.newMessageText
      };
      this._state.dialogsPage.messages.push(newMessage);
      this._state.dialogsPage.newMessageText = '';
      this.subscribe(this._state)
    },
    updateNewPostText(newPostText) {
      debugger;
      this._state.profilePage.newPostText = newPostText;
      this.subscribe(this._state);
    },
    /*updateNewPostText(newPostText) {
      debugger;
      this._state.profilePage.newPostText = newPostText;
      this.subscribe(this._state);
    },*/
    updateNewMessageText(newMessageText) {
      debugger;
      this._state.dialogsPage.newMessageText = newMessageText;
      this.subscribe(this._state);
    }
  
  }
    
  export default store;