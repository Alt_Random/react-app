import React from 'react';
import s from './Dialogs.module.css';
import Message from './Message/Message';
import DialogItem from './DialogItem/DialogItem';


const Dialogs = (props) => {
    debugger;
    let dialogElements = props.getDialogs().map(
        (d) => { return <DialogItem name={d.name} id={d.id} /> }
    );
let me = props.getMessages();
    let messagesElements = me.map(
        (m) => { return <Message mes={m.message} id={m.id} sId={m.sentId} /> }
    );

    let newMessageElement = React.createRef();
    let addMessage = () => {
        debugger;
        // let text = newMessageElement.current.value;
        props.setMesssage();
    }

    let onMessageChange = () => {
        let text = newMessageElement.current.value;
        props.updateNewMessageText(text);
    }

    return (

        <div className={s.content}>
            <div className={s.dialogs}>
                <div className={s.dialogItems}>
                    {dialogElements}
                </div>

                {/* <div>{messageElements}</div> */}
                {/* <Messages mes={props.state} /> */}

                <div className={s.messages}>
                    <div>{messagesElements}</div>
                    <div>
                        <textarea ref={newMessageElement} onChange={onMessageChange} value={props.newMessageText}  />
                        <button onClick={addMessage}>Sent</button>
                        {/* <SuperComponent callMeWhenDataChanged={this.callMeHandle} value={this.state.fromSuperComponent} /> */}
                    </div>



                    {/* <div>{messagesElements}</div>
                    <div>
                        <textarea ref={newMessageElement} cols="30" rows="10"></textarea>
                        <button onClick={addMessage}>Sent</button>
                    </div> */}

                </div>
            </div>
        </div>
    );
}

export default Dialogs;