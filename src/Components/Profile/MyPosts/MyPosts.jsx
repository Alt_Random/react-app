import React from "react";
import s from './MyPosts.module.css';
import Post from './Post/Post';


const MyPosts = (props) => {
    debugger;
    let postsElements =
    props.getPosts().map((p) => {
            return (<Post message={p.message} like={p.likesCount} />)
        });

    let newPostElement = React.createRef();

    let addPost = () => {
        debugger;
        props.setPost();
    }

    let onPostChange = () => {
        debugger;
        let text = newPostElement.current.value;
        props.updateNewPostText(text);
    }

    return (
        <div>

            <textarea ref={newPostElement} onChange={onPostChange} value={props.newPostText} />
            <button onClick={addPost}>Add</button>
            <button>Remove</button>
            {postsElements}
        </div>

    );
}
export default MyPosts;