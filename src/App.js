import React from 'react';
import './App.css';
import Header from './Components/Header/Header';
import Navbar from './Components/Navbar/Navbar';
import Footer from './Components/Footer/Footer';
import Profile from './Components/Profile/Profile';
import Dialogs from './Components/Dialogs/Dialogs';
import { Route, BrowserRouter } from 'react-router-dom';
import Music from './Components/Music/Music';
import Settings from './Settings/Settings';
import News from './Components/News/News';

const App = (props) => {
debugger;
  return (
    <BrowserRouter>
      <div className="app-wrapper">
        <Header />
        {/* <Navbar/> */}
         <Navbar getSidebars={props.store.getSidebars.bind(props.store)}/> 

        <div className="app-wrapper-content">

          <Route exact path='/dialogs' render={() => <Dialogs getMessages={props.store.getMessages.bind(props.store)} getDialogs={props.store.getDialogs.bind(props.store)} setMesssage={props.store.setMesssage.bind(props.store)} updateNewMessageText={props.store.updateNewMessageText.bind(props.store)}/>} />
          <Route path='/profile' render={() => <Profile getPosts={props.store.getPosts.bind(props.store)} setPost={props.store.setPost.bind(props.store)} updateNewPostText={props.store.updateNewPostText.bind(props.store)}/>} />
          {/* <Route exact path='/dialogs' component={Dialogs}/>
        <Route path='/profile' component={Profile}/> */}
          <Route path='/news' component={News} />
          <Route path='/music' component={Music} />
          <Route path='/settings' component={Settings} />
         
        </div>
        <Footer />
      </div>
    </BrowserRouter>
  );

}


export default App;
