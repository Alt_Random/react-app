import React from "react";
import s from './Profile.module.css';
import MyPosts from "./MyPosts/MyPosts";
import ProfileInfo from "./ProfileInfo/ProfileInfo";


const Profile = (props) => {

  return (
    <div className={s.content}>
      <main>  
        <img className={s.headerImg} alt="avatar" src="https://www.wallpaperup.com/uploads/wallpapers/2014/11/29/534103/34ffc6c07b641b3a4b9d7de33540b265-500.jpg"></img>
        <div className={s.body}>
        <ProfileInfo/>
        <MyPosts getPosts={props.getPosts} setPost={props.setPost} updateNewPostText={props.updateNewPostText}/>
        </div>
      </main>
    </div>
  );
}
export default Profile;